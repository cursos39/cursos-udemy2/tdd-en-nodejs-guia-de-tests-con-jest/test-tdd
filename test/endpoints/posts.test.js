const { posts } = require('../../src/endpoints/index')

describe('Posts endpoints', () => {
  describe('post method', () => {
    const post = {
      userId: 1,
      title: 'Test tile',
      body: 'Post Body test',
    }
    const users = [{ id: 1 }, { id: 2 }]
    const req = {
      body: post,
    }
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      sendStatus: jest.fn()
    }
    const axios = {
      get: jest.fn().mockResolvedValue({ data: users }),
      post: jest.fn().mockResolvedValue({data: {id: 100}}),
    }
    it('Should create a post', async() => {
      await posts({axios}).post(req, res)
      expect(res.status.mock.calls).toEqual([ [ 201 ] ])
      expect(res.send.mock.calls).toEqual([ [ {id: 100} ] ])
      expect(axios.get.mock.calls).toEqual([ [ 'https://jsonplaceholder.typicode.com/users' ] ])
      expect(axios.post.mock.calls).toEqual([ ['https://jsonplaceholder.typicode.com/posts', post] ])
    })
    it('Should not create if user_id does not exist', async() => {
      post.userId = 3
      await posts({axios}).post(req, res)
      expect(res.sendStatus.mock.calls).toEqual([ [ 400 ] ])
    })
  })
})
